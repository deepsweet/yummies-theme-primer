import gulp from 'gulp';

gulp.task('webpack:dev', done => {
    const gutil = require('gulp-util');
    const webpack = require('webpack');
    const WebpackDevServer = require('webpack-dev-server');
    const webpackDevConfig = require('./conf/webpack');

    const server = new WebpackDevServer(webpack(webpackDevConfig), {
        hot: true,
        stats: {
            assets: false,
            colors: true,
            version: false,
            hash: false,
            chunkModules: false,
            children: false
        }
    });

    server.listen('3000', 'localhost', function(err) {
        if (err) {
            throw new gutil.PluginError('webpack:dev', err);
        }

        gutil.log('[webpack:dev]', 'http://localhost:3000/webpack-dev-server/');

        done();
    });
});

gulp.task('default', [ 'webpack:dev' ]);
