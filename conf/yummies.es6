import path from 'path';

export default {
    styles: 'styles.less',
    layers: [
        path.resolve('node_modules/@yummies/core-components/components/'),
        path.resolve('node_modules/@yummies/theme-reset/components/'),
        path.resolve('components/'),
        path.resolve('demo/components/')
    ]
};
