import path from 'path';
import webpack from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';

import autoprefixerConfig from './autoprefixer';
import yummiesConfig from './yummies';

export default {
    cache: true,
    entry: [
        'webpack/hot/dev-server',
        './demo/index'
    ],
    output: {
        pathinfo: true,
        path: path.resolve('./'),
        publicPath: '/',
        filename: 'bundle.js'
    },
    resolve: {
        root: path.resolve('demo'),
        extensions: [ '', '.js', '.es6', '.json' ],
        alias: {
            conf: path.resolve('conf')
        }
    },
    module: {
        preLoaders: [
            {
                test: /\.es6$/,
                loader: 'babel'
            }
        ],
        loaders: [
            {
                test: /\.less$/,
                loaders: [
                    'style',
                    'css?-minimize',
                    'autoprefixer?' + autoprefixerConfig,
                    'less',
                    '@yummies/common-styles-loader?' + JSON.stringify(yummiesConfig)
                ]
            },
            {
                test: /\.json$/,
                loader: 'json'
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify(process.env.NODE_ENV)
            }
        }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin(),
        new HtmlWebpackPlugin({
            template: 'demo/assets/index.html',
            assets: {
                bundle: 'bundle.js'
            }
        })
    ]
};
