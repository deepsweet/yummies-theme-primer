import Yummies from '@yummies/yummies';
import Demo from '#demo';

Yummies.render(Demo(), document.getElementById('app'));
